#/bin/sh
COMMIT_TAG=$(git log | head -1 | awk '{print $2}' | awk '{print substr ($0, 0, 8)}')
docker build -t helloflask:$COMMIT_TAG .

