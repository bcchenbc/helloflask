#!/bin/sh
COMMIT_TAG=$(git log | head -1 | awk '{print $2}' | awk '{print substr ($0, 0, 8)}')
docker stop webservice00 && docker rm webservice00
docker run -d --name webservice00 -p 10000:80 helloflask:$COMMIT_TAG
